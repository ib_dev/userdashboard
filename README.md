# Overview #

Realtime user dashboard. Add, modify, and delete users. Suitable for huge user tables. Current one has near 1000 fields but it's not limited to it. Sample events are generated for illustration (15 for every connection).
Fully modular angular app design. Directory structure geared towards large projects. Each controller, service, filter and directive are placed in their own file. No database attached.

## Test it ##

* [Demo](http://user-dashboard.fr.openode.io)

## Most important technologies and methodology used ##

* AngularJs - JavaScript-based front-end web application framework.
* Socket.IO - JavaScript library for realtime web applications.
* Express - Minimalist web framework for Node.js.
* Sass - CSS extension language.
* BEM - Methodology, that helps you to achieve reusable components and code sharing in the front-end.
* Babel - JavaScript compiler (ES2015).
* Bower - A package manager for the web.
* Gulp - Node.js-based task runner.
* Generator-ng-poly - Yeoman generator for modular AngularJS apps with optional Polymer support

## Setup ##
1. Install [Node.js](http://nodejs.org/)
- This will also install npm.
1. Run `npm install -g bower gulp yo generator-ng-poly@0.13.0`
- This enables Bower, Gulp, and Yeoman generators to be used from command line.
1. Run `npm install` to install this project's dependencies
1. Run `bower install` to install client-side dependencies
1. Use [generator-ng-poly](https://github.com/dustinspecker/generator-ng-poly) to create additional components

## Gulp tasks ##
- Run `gulp` to start a localhost and open in the default browser
- Run `gulp build` to compile assets
- Run `gulp build --stage=prod` to concat and minify HTML, CSS, and Angular modules
