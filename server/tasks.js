

function isConnected(io, id){
  var connections = Object.keys(io.engine.clients);
  for (var i = 0; i < connections.length; i++) {
    if (connections[i] == id) return true;
  }
}

var tasks = {
  forWithDelay: function(i, length, fn, delay, io, id) {
    setTimeout(function () {
      fn(i, length);
      i++;
      if (i < length && isConnected(io, id)) {
        tasks.forWithDelay(i, length, fn, delay, io, id);
      }
    }, delay);
  },
  getData: function(obj){
    if (obj.id){
      return obj.id;
    } else {
      return obj.user;
    }
  }
};

module.exports = tasks;
