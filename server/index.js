
var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http);




require('./config')(app);

require('./routes')(app);

require('./socket')(io);


var port = app.get('port');

http.listen(port, function () {
  console.log('Server listening on %d', port);
});










