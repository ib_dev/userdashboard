

var tbUpdates = require("./json/tbUpdates.json"),
    tasks = require("./tasks.js");

module.exports = function(io, socket) {

  tasks.forWithDelay(0, tbUpdates.length, function(i){

    var event = tbUpdates[i].$type;
    socket.emit(event, tasks.getData( tbUpdates[i]) );

  }, 5000, io, socket.id);

};




