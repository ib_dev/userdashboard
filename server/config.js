var express = require('express'),
    path = require('path');

module.exports = function(app){

  app.set('port', process.env.PORT || 8080);

  app.use(express.static(path.resolve('build/app'))); // directory for static assets - images, CSS, JS

};
