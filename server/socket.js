
var table = require("./json/table.json"),
    tableUpdater = require("./tableUpdater");


module.exports = function(io){

  io.on('connection', function(socket){
    console.log('User ' + socket.id + ' connected');


    socket.on('login', function(msg) {
      socket.emit('list', table);
      tableUpdater(io, socket);
    });


    socket.on('update', function(msg) {
      console.log('update: ', msg);
      socket.broadcast.emit('update', msg);
    });
    socket.on('remove', function(msg) {
      console.log('remove: ', msg);
      socket.broadcast.emit('remove', msg);
    });
    socket.on('add', function(msg) {
      console.log('add: ', msg);
      socket.broadcast.emit('add', msg);
    });


    socket.on('disconnect', function(){
      console.log('User ' + socket.id + ' disconnected');
    });

  });

};
