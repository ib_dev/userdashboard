(function () {
  'use strict';

  class LoginCtrl {
    constructor() {
      let vm = this;
      vm.ctrlName = 'LoginCtrl';
    }
  }

  /**
   * @ngdoc object
   * @name states.login.controller:LoginCtrl
   *
   * @description
   *
   */
  angular
    .module('states.login')
    .controller('LoginCtrl', LoginCtrl);
}());
