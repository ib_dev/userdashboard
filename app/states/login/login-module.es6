(function () {
  'use strict';

  /* @ngdoc object
   * @name states.login
   * @description
   *
   */
  angular
    .module('states.login', [
      'ui.router'
    ]);
}());
