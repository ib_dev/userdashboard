(function () {
  'use strict';

  class Socket {
    constructor($state, $q) {
      this.socket = null;
      this._$q = $q;
      this._$state = $state;
    }


    get() {
      let deferred = this._$q.defer();

      this.socket = io.connect('http://localhost:8080');

      this.socket.on('connect', () => {
        console.log('log: Connected');
        this.socket.emit('login');
      });

      this.socket.on('list', (list) => {
        deferred.resolve({
          list: list,
          socket: this.socket,
          on: this.on,
          disconnect: this.disconnect,
          emit: this.emit
        });
      });


      this.socket.on("disconnect", () => {
        console.log('log: disconnect');
        this._$state.reload();
      });
      this.socket.on("error", (error) => console.log(`log: error: ${error}`) );

      return deferred.promise;
    }



    on(event, scope, callback) {
      this.socket.on(event, (...args) => {
        scope.$apply(() => {
          callback(...args);
        });
      });
    }

    emit(event, data){
      this.socket.emit(event, data);
    }

    disconnect(){
      this.socket.disconnect();
    }

  }

  /**
   * @ngdoc service
   * @name states.dashboard.service:Socket
   *
   * @description
   *
   */
  angular
    .module('states.dashboard')
    .service('Socket', Socket);
}());









