/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('DashboardTasks', () => {
  let service;

  beforeEach(module('states.dashboard'));

  beforeEach(inject((DashboardTasks) => {
    service = DashboardTasks;
  }));

  it('should equal DashboardTasks', () => {
    expect(service.get()).toEqual('DashboardTasks');
  });
});
