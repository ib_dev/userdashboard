(function () {
  'use strict';

  class DashboardTasks {
    constructor() {

    }

    _uid() {
      return new Date().getTime() + Math.floor(Math.random() * (1000 + 1));
    }

    add(arr, data){
      if (data.id == null) data.id = this._uid();
      console.log('add: ', data);
      arr.unshift(data);
    }

    remove(arr, data){
      console.log('remove: ', data);
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].id == data) arr.splice(i, 1);
      }
    }



    _updateProp(obj, change){
      let keys = Object.keys(change);
      if (keys.length > 2) return false;

      function findKey(keys){
        for (let i = 0; i < keys.length; i++) {
          if (keys[i] != 'id') return keys[i];
        }
      }
      let key = findKey(keys);
      let val = change[key];

      obj[key] = val;
    }

    update(arr, data){
      console.log('update: ', data);
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].id == data.id) this._updateProp(arr[i], data);
      }
    }




  }

  /**
   * @ngdoc service
   * @name states.dashboard.service:DashboardTasks
   *
   * @description
   *
   */
  angular
    .module('states.dashboard')
    .service('DashboardTasks', DashboardTasks);
}());
