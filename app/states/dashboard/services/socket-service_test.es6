/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Socket', () => {
  let service;

  beforeEach(module('states.dashboard'));

  beforeEach(inject((Socket) => {
    service = Socket;
  }));

  it('should equal Socket', () => {
    expect(service.get()).toEqual('Socket');
  });
});
