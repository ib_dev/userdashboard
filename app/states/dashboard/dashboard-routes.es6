(function () {
  'use strict';

  angular
    .module('states.dashboard')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('dashboard', {
        url: '/',
        templateUrl: 'states/dashboard/views/dashboard.tpl.html',
        controller: 'DashboardCtrl',
        controllerAs: 'vm',
        resolve: {
          Socket: ['Socket', (Socket) =>  Socket.get()]
        }
      });
  }
}());


