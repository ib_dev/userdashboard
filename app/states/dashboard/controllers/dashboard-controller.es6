(function () {
  'use strict';

  class DashboardCtrl {
    constructor(Socket, DashboardTasks, $scope) {
      this.list = Socket.list;
      this._Socket = Socket;
      this._DashboardTasks = DashboardTasks;
      this._$scope = $scope;

      this.onInit();
    }


    onInit(){
      let scope = this._$scope;
      let socket = this._Socket;

      socket.on('remove', scope, (msg) => {
        this._DashboardTasks.remove(this.list, msg);
      });

      socket.on('add', scope, (msg) => {
        this._DashboardTasks.add(this.list, msg);
      });

      socket.on('update', scope, (msg) => {
        this._DashboardTasks.update(this.list, msg);
      });


      scope.$on('$destroy', () => {
        socket.disconnect();
      });

    }

    update(user){
      console.log('update: ', user);
      this._Socket.emit('update', user);
    }

    remove(id){
      this._DashboardTasks.remove(this.list, id);
      this._Socket.emit('remove', id);
    }

    add(newUser){
      this._DashboardTasks.add(this.list, newUser);
      this._Socket.emit('add', newUser);
    }



  }




  /**
   * @ngdoc object
   * @name states.dashboard.controller:DashboardCtrl
   *
   * @description
   *
   */
  angular
    .module('states.dashboard')
    .controller('DashboardCtrl', DashboardCtrl);

}());
