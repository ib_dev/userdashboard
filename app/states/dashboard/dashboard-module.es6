(function () {
  'use strict';

  /* @ngdoc object
   * @name states.dashboard
   * @description
   *
   */
  angular
    .module('states.dashboard', [
      'ui.router'
    ]);
}());
