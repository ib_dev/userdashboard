(function () {
  'use strict';

  /* @ngdoc object
   * @name states
   * @description
   *
   */
  angular
    .module('states', [
      'states.dashboard',
      'states.login'
    ]);
}());
