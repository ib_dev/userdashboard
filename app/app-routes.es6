(function () {
  'use strict';

  angular
    .module('dashboard')
    .config(config);

  function config($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
  }
}());
