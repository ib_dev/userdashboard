/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('dashboardTableInputs', () => {
  let scope
    , element;

  beforeEach(module('components.dashboardTableInputs', 'components/dashboard-table-inputs/directives/dashboard-table-inputs-directive.tpl.html'));

  beforeEach(inject(($compile, $rootScope) => {
    scope = $rootScope.$new();
    element = $compile(angular.element('<dashboard-table-inputs></dashboard-table-inputs>'))(scope);
  }));

  it('should have correct text', () => {
    scope.$apply();
    expect(element.isolateScope().dashboardTableInputs.name).toEqual('dashboardTableInputs');
  });
});
