(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name components.dashboardTableInputs.directive:dashboardTableInputs
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="components.dashboardTableInputs">
       <file name="index.html">
        <dashboard-table-inputs></dashboard-table-inputs>
       </file>
     </example>
   *
   */
  angular
    .module('components.dashboardTableInputs')
    .directive('dashboardTableInputs', dashboardTableInputs);

  function dashboardTableInputs() {
    return {
      restrict: 'EA',
      scope: {},
      bindToController: {
        onAdd: "&"
      },
      templateUrl: 'components/dashboard-table-inputs/directives/dashboard-table-inputs-directive.tpl.html',
      replace: false,
      controllerAs: 'vm',
      controller: 'DashboardTableInputsCtrl',
      link(scope, element, attrs) {
        /* jshint unused:false */
        /* eslint "no-unused-vars": [2, {"args": "none"}] */
      }
    };
  }
}());
