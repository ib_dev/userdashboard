/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('DashboardTableInputsCtrl', () => {
  let ctrl;

  beforeEach(module('components.dashboardTableInputs'));

  beforeEach(inject(($rootScope, $controller) => {
    ctrl = $controller('DashboardTableInputsCtrl');
  }));

  it('should have ctrlName as DashboardTableInputsCtrl', () => {
    expect(ctrl.ctrlName).toEqual('DashboardTableInputsCtrl');
  });
});
