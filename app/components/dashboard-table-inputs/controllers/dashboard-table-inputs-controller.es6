(function () {
  'use strict';

  class DashboardTableInputsCtrl {
    constructor() {

    }

    add(newUser, form){
      this.onAdd({newUser: newUser});
      this.newUser = {};
      form.$setUntouched();
    }

  }

  /**
   * @ngdoc object
   * @name components.dashboardTableInputs.controller:DashboardTableInputsCtrl
   *
   * @description
   *
   */
  angular
    .module('components.dashboardTableInputs')
    .controller('DashboardTableInputsCtrl', DashboardTableInputsCtrl);
}());
