(function () {
  'use strict';


  class DashboardTableCtrl {
    constructor(IsValid) {
      this._IsValid = IsValid;
    }

    update(data){
      if (this._IsValid(data)) this.onUpdate({user:data});
    }

    remove(id){
      this.onRemove({id: id});
    }

  }

  /**
   * @ngdoc object
   * @name components.dashboardTable.controller:DashboardTableCtrl
   *
   * @description
   *
   */
  angular
    .module('components.dashboardTable')
    .controller('DashboardTableCtrl', DashboardTableCtrl);
}());
