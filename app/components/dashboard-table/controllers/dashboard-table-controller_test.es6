/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('DashboardTableCtrl', () => {
  let ctrl;

  beforeEach(module('components.dashboardTable'));

  beforeEach(inject(($rootScope, $controller) => {
    ctrl = $controller('DashboardTableCtrl');
  }));

  it('should have ctrlName as DashboardTableCtrl', () => {
    expect(ctrl.ctrlName).toEqual('DashboardTableCtrl');
  });
});
