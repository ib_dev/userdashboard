/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('IsValid', () => {
  let factory;

  beforeEach(module('components.dashboardTable'));

  beforeEach(inject((IsValid) => {
    factory = IsValid;
  }));

  it('should have someValue be IsValid', () => {
    expect(factory.someValue).toEqual('IsValid');
  });

  it('should have someMethod return IsValid', () => {
    expect(factory.someMethod()).toEqual('IsValid');
  });
});
