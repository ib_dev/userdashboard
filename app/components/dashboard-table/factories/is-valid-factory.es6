(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name components.dashboardTable.factory:IsValid
   *
   * @description
   *
   */
  angular
    .module('components.dashboardTable')
    .factory('IsValid', IsValid);

  function IsValid() {

    function isValid(data){
      for (let i in data) {
        if (!data[i]) return false;
      }
      return true;
    }

    return isValid;
  }
}());
