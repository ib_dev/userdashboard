/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('dashboardTable', () => {
  let scope
    , element;

  beforeEach(module('components.dashboardTable', 'components/dashboard-table/directives/dashboard-table-directive.tpl.html'));

  beforeEach(inject(($compile, $rootScope) => {
    scope = $rootScope.$new();
    element = $compile(angular.element('<dashboard-table></dashboard-table>'))(scope);
  }));

  it('should have correct text', () => {
    scope.$apply();
    expect(element.isolateScope().dashboardTable.name).toEqual('dashboardTable');
  });
});
