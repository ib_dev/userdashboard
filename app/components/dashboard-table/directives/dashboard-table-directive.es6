(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name components.dashboardTable.directive:dashboardTable
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="components.dashboardTable">
       <file name="index.html">
        <dashboard-table></dashboard-table>
       </file>
     </example>
   *
   */
  angular
    .module('components.dashboardTable')
    .directive('dashboardTable', dashboardTable);

  function dashboardTable($animate) {
    return {
      restrict: 'EA',
      scope: {},
      bindToController: {
        users: '=',
        onUpdate: '&',
        onRemove: '&'
      },
      templateUrl: 'components/dashboard-table/directives/dashboard-table-directive.tpl.html',
      replace: false,
      controllerAs: 'vm',
      controller: 'DashboardTableCtrl',
      link(scope, element, attrs) {
        /* jshint unused:false */
        /* eslint "no-unused-vars": [2, {"args": "none"}] */

        let elem = element.find('.tbody');

        elem.on("scrollstart", () => {
          $animate.enabled(elem, false);
        });

        elem.on("scrollstop", () => {
          $animate.enabled(elem, true);
        });

      }
    };
  }
}());
