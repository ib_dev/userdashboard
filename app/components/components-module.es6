(function () {
  'use strict';

  /* @ngdoc object
   * @name components
   * @description
   *
   */
  angular
    .module('components', [
      'components.dashboardTable',
      'components.dashboardTableInputs'
    ]);
}());
