/* global describe, beforeEach, it, browser, expect */
'use strict';

import DashboardTablePage from './dashboard-table.po';

describe('Dashboard table page', () => {
  let dashboardTablePage;

  beforeEach(() => {
    dashboardTablePage = new DashboardTablePage();
    browser.get('/#/dashboard-table');
  });

  it('should say DashboardTableCtrl', () => {
    expect(dashboardTablePage.heading.getText()).toEqual('dashboardTable');
    expect(dashboardTablePage.text.getText()).toEqual('DashboardTableCtrl');
  });
});
