/* global describe, beforeEach, it, browser, expect */
'use strict';

import TableInputsPage from './table-inputs.po';

describe('Table inputs page', () => {
  let tableInputsPage;

  beforeEach(() => {
    tableInputsPage = new TableInputsPage();
    browser.get('/#/table-inputs');
  });

  it('should say TableInputsCtrl', () => {
    expect(tableInputsPage.heading.getText()).toEqual('tableInputs');
    expect(tableInputsPage.text.getText()).toEqual('TableInputsCtrl');
  });
});
